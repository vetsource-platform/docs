## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Hugo

This site is built using [Hugo](https://gohugo.io), which a static site generator. Basically, you create all the content in markdown (or html) files and Hugo builds the site as static html files. All of the content for the site is located in `docs/content/en` with the OpenAPI spec located in `static/api/openapi.yaml`.

You organize your content in directories with each directory containing at least an `\_index.md` file. The sidebar on the left side of the docs page is automatically created by hugo when the site is built. It does this by traversing all the directories in your content folder and looking for the `\_index.md` files and any other files in your subdirectories.

Read more at Hugo's [documentation](https://gohugo.io/documentation/).

### Configuration

Basic configuration of the site is done in the `config.toml` file. The most important setting for that file is this one:
`baseURL = "https://vetsource-platform.gitlab.io/docs"`

That needs to be set correctly for the site to work both locally and in Gitlab Pages. Don't change it unless you know what you're doing.

There are configuration variables that are global hugo settings, but also variables specific to the [docsy theme](https://www.docsy.dev/docs/getting-started/#basic-site-configuration).

### Templates

Templates are a bit complicated but you shouldn't have to worry too much about them since they're already set up for this site. If you want more information, you can find it [here](https://gohugo.io/templates/).

### Themes

Hugo uses [themes](https://themes.gohugo.io/) to control how content is rendered, organized, etc. We are using the [docsy theme](https://www.docsy.dev/).

### Metadata

Each page in the site has metadata that controls how titles appear, how pages are sorted in the sidebar, etc.

Here's an example:

```
---
title: "Use Cases"
linkTitle: "Use Cases"
weight: 300
date: 2020-05-22
description: >
  Building solutions using the Vetsource Platform API
---
```

The `weight` field is used for sorting. All content in the same directory will be sorted relative to each other based on the `weight`. The higher the number, the lower in the sidebar the page will appear.

Note: the weight in the `_index.md` files is used to sort peer directories relative to each other. The weight for other content is used to sort the files *within* a directory itself.


### Mermaid

Diagrams are created using [mermaid](https://mermaid-js.github.io/mermaid/). The site is currently configured such that mermaid diagrams will not work unless you set `mermaid: true` in a page's metadata. This is so that the mermaid javascript is only loaded when needed for performance reasons.

## CI/CD

This site is built and deployed using gitlab's CI/CD feature. The build steps are found in .gitlab-ci.yml. It is hosted via [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Update submodules for hugo themes: `git submodule update --init --recursive`
1. Install [Hugo](https://gohugo.io/getting-started/installing/). Note: you must install the "extended" version of hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

### Preview the site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from [Docsy](https://www.docsy.dev/)

## Updating the OpenAPI spec

To update the OpenAPI spec, replace `static/api/openapi.yaml` with the latest.