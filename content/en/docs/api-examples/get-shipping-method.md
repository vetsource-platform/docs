---
title: "Get Shipping Method"
linkTitle: "Get Shipping Method"
weight: 100
date: 2020-04-01
description: >
  Return valid shipping options for an order based on the order items and shipping address
---

**Important Information:** 
- This sample call will require a valid practiceApiId provided by Vetsource.  
- Click here for [Detailed Shipping Options spec](/openapi/#/default/post_order_shippingOptions_)  
   
**URL:** POST  *vetsource_supplied_url*/order/shippingOptions/  
**Body:** 
```
{
  "orderId": "EXTERNAL_ORDER_ID",
  "client": {
    "clientId": "EXTERNAL_CLIENT_ID",
    "practiceId": "EXTERNAL_PRACTICE_ID",
    "practiceApiId": "5a7828bf-69f6-4f78-9fd6-52f5c7c1cc88",
    "firstName": "Carrie",
    "lastName": "Page",
    "email": "carrie_page@notanemail.com",
    "phone": 9185551212
  },
  "shippingAddress": {
    "addressId": "EXTERNAL_ADDRESS_ID",
    "address1": "1316 N Dixie Boulevard",
    "address2": "",
    "city": "Odessa",
    "state": "TX",
    "zip": 79761,
    "zip4": ""
  },
  "items": [
    {
      "orderItemId": "EXTERNAL_ORDERITEM_ID",
      "petId": "EXTERNAL_PET_ID",
      "quantity": 1,
      "skuId": "21432576MD"
    }
  ]
  }
}
```  
**Response:** 
```
{
    "orderId": "EXTERNAL_ORDER_ID",
    "shipments": [
        {
            "items": [
                "EXTERNAL_ORDERITEM_ID"
            ],
            "shippingClass": "default",
            "shippingMethods": [
                {
                    "shippingMethodId": 1,
                    "methodName": "Standard (3-6 business days)",
                    "shippingCost": 6.95,
                    "perItem": false
                },
                {
                    "shippingMethodId": 4,
                    "methodName": "3 Business days",
                    "shippingCost": 12.95,
                    "perItem": false
                },
                {
                    "shippingMethodId": 5,
                    "methodName": "2 Business days",
                    "shippingCost": 15.95,
                    "perItem": false
                },
                {
                    "shippingMethodId": 6,
                    "methodName": "Next Business Day PM",
                    "shippingCost": 19.95,
                    "perItem": false
                }
            ],
            "shippingAllowed": true,
            "deniedReason": ""
        }
    ]
}
```