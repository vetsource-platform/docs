---
title: "Create Order"
linkTitle: "Create Order"
weight: 300
date: 2020-04-01
description: >
  Create an Order
---

**Important Information:** 
- This sample call will require a valid practiceApiId provided by Vetsource.  
- If objects do not exist for an External ID, the object will be created in Vetsource using information in the API call.  
- orderId must be incremented if this message is used again  
- Click here for [Detailed Create Order spec](/openapi/#/default/post_order_placeOrder_)  

**URL:** POST  *vetsource_supplied_url*/order/placeOrder/

**Body:** 
```
{
    "orderId": "EXTERNAL_ORDER_ID",
    "client": {
        "practiceId": "EXTERNAL_PRACTICE_ID",
        "practiceApiId": "5a7828bf-69f6-4f78-9fd6-52f5c7c1cc88",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "billingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "pets": [
        {
            "petName": "Kristi",
            "petId": "EXTERNAL_PET_ID",
            "animalType": "CANINE",
            "breed": "Poodle",
            "birthDate": "2014-12-25",
            "sex": "Female",
            "neutered": true,
            "medications": "takes medication",
            "info": "allergic to cats",
            "weight": 22.5
        }
    ],
    "items": [
        {
            "orderItemId": "EXTERNAL_ORDERITEM_ID",
            "petId": "EXTERNAL_PET_ID",
            "quantity": 1,
            "skuId": "21432576MD",
            "shippingMethodId": 1
        }
    ]
}
```
**Response:** 
```
{
    "orderId": "EXTERNAL_ORDER_ID",
    "orderApiId": "3caba191-6cf1-4482-bd05-e319763ab509",
    "status": "N",
    "orderTotal": 0,
    "orderTax": 0,
    "shippingAmount": 0,
    "client": {
        "practiceApiId": "5a7828bf-69f6-4f78-9fd6-52f5c7c1cc88",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "1f5132dd-65a7-415b-a789-9b0e3bd3059d",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "addressApiId": "aa592f99-6354-4a2e-babe-c5cba9b53d93",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "billingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "addressApiId": "aa592f99-6354-4a2e-babe-c5cba9b53d93",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "pets": [
        {
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "petName": "Kristi",
            "petId": "EXTERNAL_PET_ID",
            "animalType": "CANINE",
            "breed": "Poodle",
            "birthDate": "2014-12-24",
            "sex": "Female",
            "neutered": true,
            "medications": "takes medication",
            "info": "allergic to cats",
            "weight": 22.5
        }
    ],
    "items": [
        {
            "orderItemApiId": "4522987d-f314-4d80-a2ca-bb0193f03ec8",
            "orderItemId": "EXTERNAL_ORDERITEM_ID",
            "petId": "EXTERNAL_PET_ID",
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "price": 0,
            "quantity": 1,
            "itemTax": 0,
            "skuId": "21432576MD",
            "shippingMethodId": 1,
            "trackingNumber": "",
            "itemStatus": "Processing",
            "skuName": "Duralactin Canine 60-ct Chewable Tablets Bottle"
        }
    ]
}
```