---
title: "Get Scriptshare"
linkTitle: "Get Scriptshare"
weight: 700
date: 2020-05-22
description: >
  Retrieve a Scriptshare using the Vetsource ApiId
---
Click here for [Detailed Get Scriptshare spec](/openapi/#/default/get_scriptright_cart_scriptshare_)  
    
**URL:** GET  *vetsource_supplied_url*/order/EXTERNAL_ORDER_ID/?scriptRightCartApiId=54a79b90-cd3b-416a-a575-f447c64ea361
**Response:**  
```
{
    "scriptRightCartApiId": "54a79b90-cd3b-416a-a575-f447c64ea361",
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "addressApiId": "d309ddb6-cd0d-4059-9341-a35843b3fdf8",
        "address1": "1614 N Dixie Boulevard",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": "1403"
    },
    "client": {
        "practiceApiId": "f80c002f-891f-4e01-8e59-699b16237838",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "81dc371e-3514-4098-991d-5da5ece64926",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "wallet": {},
    "status": "UNPROCESSED",
    "items": [
        {
            "scriptRightCartItemApiId": "17913e4b-ee1b-4992-9719-fb068ef58778",
            "pet": {
                "petApiId": "3a273140-5404-4209-b7b9-764fb050f0ec",
                "petName": "Kristi",
                "petId": "EXTERNAL_PET_ID",
                "animalType": "CANINE",
                "breed": "Poodle",
                "birthDate": "2014-12-24",
                "sex": "Female",
                "neutered": true,
                "medications": "takes medication",
                "info": "allergic to cats",
                "weight": 22.5
            },
            "veterinarian": {
                "firstName": "Alan",
                "lastName": "Miller",
                "licenseNumber": "324810",
                "licenseState": "WA",
                "deaNumber": "not exposed",
                "expiration": "2024-12-08 06:00:00 UTC",
                "pin": "1234",
                "vetApiId": "b40cd205-de3f-483e-82d6-861de82725d0",
                "vetId": "EXTERNAL_VET_ID",
                "email": "amiller@nomail.com"
            },
            "skuId": "21432576MD",
            "quantity": 1,
            "price": 0,
            "shippingCost": 0,
            "refills": 0,
            "refillExpiration": "2020-12-08 06:00:00 UTC",
            "directions": "take two tablets in the morning",
            "autoShipStartDate": "2020-05-18 05:00:00 UTC",
            "shippingModeId": 0,
            "frequency": 0,
            "pharmacyNotes": "Divide into two bottles"
        }
    ],
    "scriptShare": true,
    "scriptShareDate": "2020-05-18 05:00:00 UTC",
    "scriptShareMailCount": 0,
    "scriptShareStatus": "IN PROGRESS"
}
```