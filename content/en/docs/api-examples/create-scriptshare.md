---
title: "Create Scriptshare"
linkTitle: "Create Scriptshare"
weight: 600
date: 2020-05-22
description: >
  Create a new Scriptshare
---
**Important Information:** 
- This sample call will require a valid practiceApiId provided by Vetsource. 
- If objects do not exist for an External ID, the object will be created in Vetsource using information in the API call
- scriptShareDate parameter is used to define when the first Scriptshare e-mail is sent to the client 
- Click here for [Detailed Create Scriptshare spec](/openapi/#/default/post_scriptright_cart_scriptshare_)  
    
**URL:** POST  *vetsource_supplied_url*/scriptright/cart/scriptshare/  
**Body:**  
```
{
  "shippingAddress": {
    "addressId": "EXTERNAL_ADDRESS_ID",
    "address1": "1614 N Dixie Boulevard",
    "address2": null,
    "city": "Odessa",
    "state": "TX",
    "zip": 79761,
    "zip4": 1403
  },
  "client": {
    "clientId": "EXTERNAL_CLIENT_ID",
    "practiceApiId": "f80c002f-891f-4e01-8e59-699b16237838",
    "firstName": "Carrie",
    "lastName": "Page",
    "email": "carrie_page@notanemail.com",
    "phone": "9185551212"
  },
  "items": [
    {
      "pet": {
        "petName": "Kristi",
        "petId": "EXTERNAL_PET_ID",
        "animalType": "CANINE",
        "breed": "Poodle",
        "birthDate": "2014-12-25 00:00:00 UTC",
        "sex": "Female",
        "neutered": true,
        "medications": "takes medication",
        "info": "allergic to cats",
        "weight": 22.5
      },
      "veterinarian": {
        "vetId": "EXTERNAL_VET_ID",
        "firstName": "Alan",
        "lastName": "Miller",
        "licenseNumber": 324810,
        "licenseState": "WA",
        "deaNumber": 119,
        "expiration": "2024-12-09 00:00:00 UTC",
        "pin": 1234,
        "email": "amiller@nomail.com"
      },
      "skuId": "21432576MD",
      "quantity": 1,
      "refills": 12,
      "refillExpiration": "2020-12-09 00:00:00 UTC",
      "directions": "take two tablets in the morning",
      "pharmacyNotes": "Divide into two bottles"
    }
  ],
  "scriptShare": false,
  "scriptShareDate": "2020-05-14 00:00:00 UTC"
}
```


**Response:**   
```
{
    "scriptRightCartApiId": "fc091de3-aad0-4858-ae8a-b751aef32577",
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "addressApiId": "e49caa57-1094-43a5-9b3d-271734fa0ca9",
        "address1": "1614 N Dixie Boulevard",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": "1403"
    },
    "client": {
        "practiceApiId": "f80c002f-891f-4e01-8e59-699b16237838",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "81dc371e-3514-4098-991d-5da5ece64926",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "wallet": {},
    "status": "UNPROCESSED",
    "items": [
        {
            "scriptRightCartItemApiId": "d47f3c89-8c03-41e9-bc7a-33d1ad53675b",
            "pet": {
                "petApiId": "a9092e8b-2d1d-40d8-a739-c5f0b2bd8470",
                "petName": "Kristi",
                "petId": "EXTERNAL_PET_ID",
                "animalType": "CANINE",
                "breed": "Poodle",
                "birthDate": "2014-12-24",
                "sex": "Female",
                "neutered": true,
                "medications": "takes medication",
                "info": "allergic to cats",
                "weight": 22.5
            },
            "veterinarian": {
                "firstName": "Alan",
                "lastName": "Miller",
                "licenseNumber": "324810",
                "licenseState": "WA",
                "deaNumber": "not exposed",
                "expiration": "2024-12-08 06:00:00 UTC",
                "pin": "1234",
                "vetApiId": "b40cd205-de3f-483e-82d6-861de82725d0",
                "vetId": "EXTERNAL_VET_ID",
                "email": "amiller@nomail.com"
            },
            "skuId": "21432576MD",
            "quantity": 1,
            "price": 0,
            "shippingCost": 0,
            "refills": 0,
            "refillExpiration": "2020-12-08 06:00:00 UTC",
            "directions": "take two tablets in the morning",
            "autoShipStartDate": "2020-05-18 05:00:00 UTC",
            "shippingModeId": 0,
            "frequency": 0,
            "pharmacyNotes": "Divide into two bottles"
        }
    ],
    "scriptShare": true,
    "scriptShareDate": "2020-05-18 05:00:00 UTC",
    "scriptShareMailCount": 0,
    "scriptShareStatus": "IN PROGRESS"
}
``` 