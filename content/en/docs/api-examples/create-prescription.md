---
title: "Create Prescription"
linkTitle: "Create Prescription"
weight: 200
date: 2020-04-01
description: >
  Create a Prescription
---

**Important Information:** 
- This sample call will require a valid practiceApiId provided by Vetsource. 
- If objects do not exist for an External ID, the object will be created in Vetsource using information in the API call
- rxId must be incremented if this message is used again 
- Click here for [Detailed Create Prescription spec](/openapi/#/default/post_prescription_)  
   
**URL:** POST  *vetsource_supplied_url*/prescription/  
**Body:**
```
{
    "prescriptionId": "EXTERNAL_PRESCRIPTION_ID",
    "client": {
        "practiceId": "EXTERNAL_PRACTICE_ID",
        "practiceApiId": "0ebd97b8-1b20-409c-8918-795a6c5726e2",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "pet": {
            "petName": "Kristi",
            "petId": "EXTERNAL_PET_ID",
            "animalType": "CANINE",
            "breed": "Poodle",
            "birthDate": "2014-12-25",
            "sex": "Female",
            "neutered": true,
            "medications": "takes medication",
            "info": "allergic to cats",
            "weight": 22.5
        },
	"vet": {
            "vetId": "EXTERNAL_VET_ID",
            "firstName": "Alan",
            "lastName": "Miller",
            "licenseNumber": 324810,
            "licenseState": "WA",
            "deaNumber": 119,
            "expiration": "2024-12-09 00:00:00 UTC",
            "pin": 1234,
            "email": "amiller@noemail.com"
        },
        "skuId": "21432576MD",
        "fillQuantity": 1,
        "usageInstructions": "Take one tablet twice daily",
        "pharmacyNotes": "Take on an empty stomach",
        "refills": 3,
        "writtenDate": "2019-12-09 00:00:00 UTC",
        "expirationDate": "2020-12-09 00:00:00 UTC"
}
```
**Response:** 
```
{
    "prescriptionApiId": "24f1794d-0cbe-4d96-8743-13eb34cb3d3a",
    "skuId": "21432576MD",
    "client": {
        "practiceApiId": "0ebd97b8-1b20-409c-8918-795a6c5726e2",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "8269b05c-4695-4441-a3a4-eb01686b7159",
        "email": "carrie_page@notanemail.com"
    },
    "shippingAddress": {
        "addressId": "EXTERNAL_ADDRESS_ID",
        "addressApiId": "578cdf4d-5eec-49fd-8269-986c1e823ebb",
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761",
        "zip4": ""
    },
    "pet": {
        "petApiId": "08a9a76e-e468-4e62-b3cb-0ea90574f635",
        "petName": "Kristi",
        "petId": "EXTERNAL_PET_ID",
        "animalType": "CANINE",
        "breed": "Poodle",
        "birthDate": "2014-12-24",
        "sex": "Female",
        "neutered": true,
        "medications": "takes medication",
        "info": "allergic to cats",
        "weight": 22.5
    },
    "vet": {
        "firstName": "Alan",
        "lastName": "Miller",
        "licenseNumber": "324810",
        "licenseState": "WA",
        "deaNumber": "not exposed",
        "expiration": "2024-12-08 06:00:00 UTC",
        "pin": "1234",
        "vetApiId": "d5acd6f2-302d-42e6-8290-3b15e4c0c7e1",
        "vetId": "EXTERNAL_VET_ID",
        "email": "amiller@noemail.com"
    },
    "fillQuantity": 1,
    "usageInstructions": "TAKE ONE TABLET TWICE DAILY",
    "pharmacyNotes": "Take on an empty stomach",
    "refills": 3,
    "writtenDate": "2020-02-11 06:00:00 UTC",
    "expirationDate": "2020-12-09 00:00:00 UTC",
    "fillNumber": 0,
    "fillsRemaining": 4,
    "unitsRemaining": 4,
    "compounded": false
}
```