---
title: "Get Order Status"
linkTitle: "Get Order Status"
weight: 500
date: 2020-04-01
description: >
  Get the status of an order using your order ID
---

Click here for [Detailed Get Status using External Order ID spec](/openapi/#/default/get_order__externalOrderId__)  
    
**URL:** GET  *vetsource_supplied_url*/order/EXTERNAL_ORDER_ID/  
**Response:**  
```
{
    "orderId": "EXTERNAL_ORDER_ID",
    "orderApiId": "3caba191-6cf1-4482-bd05-e319763ab509",
    "placedDate": "2020-02-01 19:44:23 UTC",
    "status": "W",
    "orderTotal": 0,
    "orderTax": 0,
    "shippingAmount": 0,
    "client": {
        "practiceApiId": "5a7828bf-69f6-4f78-9fd6-52f5c7c1cc88",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "1f5132dd-65a7-415b-a789-9b0e3bd3059d",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "shippingAddress": {
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761"
    },
    "billingAddress": {
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761"
    },
    "pets": [
        {
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "petName": "Kristi",
            "petId": "EXTERNAL_PET_ID",
            "animalType": "CANINE",
            "breed": "Poodle",
            "birthDate": "2014-12-24",
            "sex": "Female",
            "neutered": true,
            "medications": "takes medication",
            "info": "allergic to cats",
            "weight": 22.5
        }
    ],
    "items": [
        {
            "orderItemApiId": "4522987d-f314-4d80-a2ca-bb0193f03ec8",
            "petId": "EXTERNAL_PET_ID",
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "price": 0,
            "quantity": 1,
            "itemTax": 0,
            "skuId": "21432576MD",
            "shippingMethodId": 1,
            "trackingNumber": "",
            "itemStatus": "Processing",
            "skuName": "Tramadol Tablets 50 mg 60-ct Vial"
        }
    ]
}
```

#### Get Order Status using Vetsource Order ID  
**Purpose:** Get the status of an order using the Vetsource Order ID  

Click here for [Detailed Get Status using Vetsource Order ID spec](/openapi/#/default/get_order_apiId__apiId__)  
   
**URL:** POST  *vetsource_supplied_url*/order/apiId/3caba191-6cf1-4482-bd05-e319763ab509/  
**Response:**   
```
{
    "orderId": "EXTERNAL_ORDER_ID",
    "orderApiId": "3caba191-6cf1-4482-bd05-e319763ab509",
    "placedDate": "2020-02-01 19:44:23 UTC",
    "status": "W",
    "orderTotal": 0,
    "orderTax": 0,
    "shippingAmount": 0,
    "client": {
        "practiceApiId": "5a7828bf-69f6-4f78-9fd6-52f5c7c1cc88",
        "firstName": "Carrie",
        "lastName": "Page",
        "clientId": "EXTERNAL_CLIENT_ID",
        "clientApiId": "1f5132dd-65a7-415b-a789-9b0e3bd3059d",
        "email": "carrie_page@notanemail.com",
        "phone": "9185551212"
    },
    "shippingAddress": {
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761"
    },
    "billingAddress": {
        "address1": "1316 N Dixie Boulevard",
        "address2": "string",
        "city": "Odessa",
        "state": "TX",
        "zip": "79761"
    },
    "pets": [
        {
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "petName": "Kristi",
            "petId": "EXTERNAL_PET_ID",
            "animalType": "CANINE",
            "breed": "Poodle",
            "birthDate": "2014-12-24",
            "sex": "Female",
            "neutered": true,
            "medications": "takes medication",
            "info": "allergic to cats",
            "weight": 22.5
        }
    ],
    "items": [
        {
            "orderItemApiId": "4522987d-f314-4d80-a2ca-bb0193f03ec8",
            "petId": "EXTERNAL_PET_ID",
            "petApiId": "797468b4-55ba-40d4-a369-777aa9ebb0a5",
            "price": 0,
            "quantity": 1,
            "itemTax": 0,
            "skuId": "21432576MD",
            "shippingMethodId": 1,
            "trackingNumber": "",
            "itemStatus": "Processing",
            "skuName": "Tramadol Tablets 50 mg 60-ct Vial"
        }
    ]
}
``` 