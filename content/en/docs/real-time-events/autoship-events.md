---
title: "Autoship Events"
linkTitle: "Autoship Events"
weight: 300
date: 2020-04-01
description: >
  Tracking real time Autoship events
---

Autoship events provide important information relating to the status of an Autoship.  These events provide the pet owner visibility into the state of an autoship as well as possible issues that could disrupt a scheduled shipment.

#### Sample events

With the event mechanism, you can receive notifications on the following Autoship events:

* Autoship Cancellation
* Autoship Credit Card Failure
* Autoship 7 day notice
* Autoship Price Change
* Autoship Credit Card Expiration 1 Month
* Autoship Credit Card Expiration 0 Month

Sample Autoship Event Response:
```
{
    "eventResponse": {
      "shippingCity": "BLOOMINGTON",
      "eventDateTime": "2017-01-18 11:32:47.482",
      "orderNumber": "633949",
      "autoShipCreditCardExp": "0719",
      "eventAction": "AUTO_SHIP_CANCELLATION",
      "autoShipCreditCard": "9999",
      "lineItems": [
        {
          "quantity": 1,
          "shippingCost": null,
          "totalPrice": 0,
          "cancellationReason": null,
          "discountAmount": null,
          "taxes": null,
          "subTotal": null,
          "productName": "ROYAL CANIN VETERINARY DIET Canine Gastrointestinal Puppy - dry Puppy Formula 8.8 lbs Bag",
          "hospitalDescription": "Vetsource Animal Hospital",
          "petName": null,
          "hospitalExternalId": "HOSP External ID",
          "hospitalNumber": 704209,
          "orderItemNumber": "78777",
          "discountReason": null,
          "rxRefillCount": null,
          "hospitalExternalRef1": "5037",
          "hospitalExternalRef2": "BOO",
          "trackingNumber": null
        }
      ],
      "shippingCountry": "US",
      "customerEmail": "alanmiller@nomail.com",
      "billingCountry": "US",
      "customerLastName": "Miller",
      "shippingZip": "55431",
      "billingZip": "55431",
      "orderSource": "Ecommerce",
      "readAttempts": "1",
      "autoShipStatus": "-",
      "customerExternalId": "39c13588-87bd-e611-8ca6-9cb65412e9b9",
      "messageId": "AQEB7YCVW3lESUjJ/eEdMgHpamUj+iFd9SeskbP7j8bnUIBtutCYlfHTSS3RID+0+lT1Pp7Zx8T3F1cKkLM5DCB5KZaNiAAZB5CafbADrP89ntYSfDoYyyGJ+DG97ZQxKY80ojILnwtH4T7SbrzPqPV2p4mQvBv2Z7TTUo+WYQhL/QcgVQFKXwO+iZRCxyPchPysq/l7BkiGJCl4tl3AO8rtxjkCqeoz73+fjphmcoAmMrwvo0QA87YKs/oLDfxTqNo4GXMJLtwTE2BgSgYKJvYqsaVuuHoDQ55x9jxs3bfmD1E+wVUTc8dUFgTiNq2EzTwLpszcJb2zPbc30+qwl4FcSzsiBPUACZy0nBH3jhnBduUlb0g6nKcoze2FXLIqOXqIQRDR7uvFAm4rQ9KSQYIkZQ==",
      "billingState": "MN",
      "eventType": "AUTOSHIP",
      "shippingTotal": null,
      "customerFirstName": "Alan",
      "shippingAddress": "9809 YORK CURV S",
      "shippingState": "MN",
      "billingAddress": "9809 YORK CURV S",
      "billingCity": "BLOOMINGTON"
    }
  }
```