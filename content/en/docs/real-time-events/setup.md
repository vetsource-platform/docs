---
title: "Setup"
linkTitle: "Setup"
weight: 100
date: 2020-04-01
description: >
  Setting up real-time event notifications
---

If you'd like to subscribe to real-time events, please contact your [Technical Contact](/docs/working-with-vetsource/contacts).  They will set up a your message queue and provide you with access information.

### Configuration

You can enable and disable specific events that you would like to receive including:

* [Orders Events](/docs/real-time-events/order-events)  
* [Autoship Events](/docs/real-time-events/autoship-events)
* [ScriptShare Events](/docs/real-time-events/scriptshare-events)
* [Prescription Events](/docs/real-time-events/prescription-events)

### Consume Events  

[Consume Events](/docs/real-time-events/consuming-events) using the API


