---
title: "Real-time Events"
linkTitle: "Real-time Events"
weight: 600
date: 2020-04-01
description: >
  Getting real-time event notifications with guaranteed delivery
---

We support real-time events with guaranteed delivery so that you can control customer communication and update your own system throughout the lifecycle of orders and autoships.
