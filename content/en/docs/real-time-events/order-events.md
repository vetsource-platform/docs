---
title: "Order Events"
linkTitle: "Order Events"
weight: 200
date: 2020-04-01
description: >
  Tracking real-time order status
---

You can track realtime status of an order throughout its lifecycle through Order Events.  This gives you the ability to take action on any and all events in your system.  For example, if you own your own PIM, you may want to use Order events as a writeback mechanism.  You could also use Order events to send your own notifications to your customers.  We provide a simple mechanism for this via our Order Event API.

These events are a very powerful mechanism you can use to simplify your workflow for your customers.  For example, if orders need prescription approval, you can route that approval however you'd like to ensure that veterinarians approve orders as quickly and easily as possible.  While we offer tools to manage approval workflow (e.g. ScriptRight), the Vetsource Platform gives you the capability to integrate seamlessly with your existing tools as well.

#### Sample events

With the event mechanism, you can receive notifications on the following Order events:

* Order Placed
* Order Item Awaiting Approval
* Order Item Canceled
* Order Canceled
* Order Item Shipped
* Order Item Approved
* Order Item Denied
* Order Item Refund
* Order Item Refund
* Order Patient Hospital Mismatch

Sample Order Event Response:
```
 {
    "eventResponse": {
      "shippingCity": "NAVARRE",
      "eventDateTime": "2017-01-18 11:29:51.12",
      "orderNumber": "999123999",
      "eventAction": "ORDER_PLACED",
      "autoShip": "Yes",  
      "lineItems": [
        {
          "quantity": 1,
          "shippingCost": 0,
          "totalPrice": 45.89,
          "cancellationReason": "",
          "discountAmount": 8.1,
          "taxes": 3.21,
          "subTotal": 45.89,
          "productName": "Rejuvenate Chewable Tablets Dogs 120-ct Tub",
          "productSku": "BHC00120TB",  
          "hospitalDescription": "Vetsource Animal Hospital",
          "petName": "Akyra",
          "externalPetId": "47912231",   
          "hospitalExternalId": "HOSP External ID",
          "hospitalNumber": 704209,
          "orderItemNumber": "78777",
          "discountReason": "",
          "rxRefillCount": 0,
          "hospitalExternalRef1": "7336",
          "hospitalExternalRef2": "DST",
          "trackingNumber": "",
          "dvmFirstName": "",
          "dvmId": "",
          "dvmLastName": "",
        }
      ],
      "shippingCountry": "US",
      "customerEmail": "alanmiller@nomail.com",
      "billingCountry": "US",
      "customerLastName": "Miller",
      "shippingZip": "32566",
      "billingZip": "32566",
      "orderSource": "Ecommerce",
      "readAttempts": "1",
      "customerExternalId": "f1d62c6a-ca0b-df11-b25c-001125573ba1",
      "messageId": "AQEBGdNPsDpVh5DK1NnDPcjfx/jBkUHwhhydnHFgJYaRgUr7YVRnIgjjF+mhhh4rC0tMdnl3tBc5yAww9SWgxEKAFA8wJNW/XmTWCu08XdQ9GPC8Ln3jUAK5O24+YhHATCmTRs3R2wJmzKEo1K7DeCW/hphlc8D3olBBEbUr3K4MjUWCSG0Ropv4phEq8PUltGt3tgh7S8SpQ9+bnT9yhnygN0v9v1dy4750/twCfGAPvoPsCXTaqKqGijrARcDS7mghQag575BHs/v3IBVciMWsty6RGIQ29YZWhzV4QJ6VAaUhHDN0SA6Sp5a/f+57lw5/HT8vxsY8FUEEutZNpfdLyKMr2+G2iMUk7aitOlOWfMTKTd1bTXbLXHfyeNhKX3lKPG3pJwpVpFVlsOAmEJMdSw==",
      "billingState": "FL",
      "eventType": "ORDER",
      "orderTotal": 49.1,
      "shippingTotal": 0,
      "customerFirstName": "Alan Miller",
      "shippingAddress": "508 S Boston",
      "shippingState": "FL",
      "externalOrderNumber": "",
      "billingAddress": "508 S Boston",
      "orderDate": "20170118T112950-06",
      "billingCity": "NAVARRE"
    }
  }
  ```

 Following fields are populated for Order Item Approved or Denied events:
* dvmId
* dvmFirstName
* dvmLastName
