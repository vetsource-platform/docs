---
title: "Consuming Events"
linkTitle: "Consuming Events"
weight: 400
date: 2020-04-01
mermaid: true
description: >
  Retrieving and acknowledging events
---

Consuming events is relatively straightforward and consists of REST calls to receive and delete messages.

### Workflow

{{<mermaid>}}
sequenceDiagram
    participant Your System
    participant Event API
    Your System ->> Event API: Receive Messages
    Event API --x Your System: Messages
    
    loop For each message
        Your System ->> Your System: Process Message
        Your System ->> Event API: Delete Message
    end
{{< /mermaid >}}

### Retrieve Message

As events occur within the Vetsource system, these events are stored in an event message queue.  

The following REST call retrieves all unread events from the queue:  

`GET *vetsource_supplied_url*/message/{GroupId}/{QueueName}`

*Note: GroupId and QueueName will be provided by your Vetsource Technical Contact*

The response will return data as described in [Order Events](/docs/real-time-events/order-events), [Autoship Events](/docs/real-time-events/autoship-events), [ScriptShare Events](/docs/real-time-events/scriptshare-events) and [Prescription Events](/docs/real-time-events/prescription-events)

### Remove Read Messages

If an event is not deleted within 1 hour of being retrieved, it will be marked as unread and become visible again.  Once an event is retrieved and processed it should be removed from the queue.

The following REST call will remove a message from the event queue:  
`POST *vetsource_supplied_url*/message/delete/{PracticeGroupId}/{QueueName}`

The body of this POST REST call will be the messageId included in the retrieved message.
