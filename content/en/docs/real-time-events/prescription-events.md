---
title: "Prescription Events"
linkTitle: "Prescription Events"
weight: 200
date: 2020-04-01
description: >
  Tracking real-time prescription status
---

You can track when a prescription is created (or deactivated) through Prescription Events.  This gives you the ability to take action on any and all events in your system.  For example, if you own your own PIM, you may want to use Prescription events as a writeback mechanism. We provide a simple mechanism for this via our Prescription Event API.

#### Sample events

With the event mechanism, you can receive notifications on the following Order events:

* Prescription Created
* Prescription Deactivated
* Prescription Patient Hospital Mismatch

Sample Prescription Event Response:
```
     {
        "eventResponse": {
            "animalType": "CANINE",
            "billingAddress": null,
            "billingCity": null,
            "billingCountry": null,
            "billingState": null,
            "billingZip": null,
            "breed": "Other/mixed",
            "compounded": false,
            "customerEmail": "noemail@vetsource.com",
            "customerExternalId": null,
            "customerFirstName": "Testy",
            "customerLastName": "Tester",
            "eventAction": "PRESCRIPTION_CREATED",
            "eventDateTime": "2022-06-14 16:00:23.836",
            "eventType": "PRESCRIPTION",
            "expirationDate": "20230613T230000Z",
            "fillNumber": 0,
            "fillQuantity": 1,
            "fillsRemaining": 12,
            "hospitalDescription": null,
            "hospitalExternalId": null,
            "hospitalExternalRef1": null,
            "hospitalExternalRef2": null,
            "hospitalNumber": 40001081,
            "lineItems": null,
            "messageId": "AQEBkx8UgGd4Xr4d7gvXwLUdPwsOy1k9UTyDXHHAbJLuaYGZbZbYPIL3TRCKUVW2s9vG754FBMkIaREjPqHwM3ijF6oQHJiROr9i3hnFADpCmcTfB/UHux9wWliMsjrrHVHSUQvHa+ZXpagjo/Ojulyr2mHtV35lkb5pHvD2WRtYBv/DFOHkCJ9WPz+OaclRxp92/vGgkmYOX6O44gN+7+pZ/g/CAFZ/as3ke0gK0ANW/5dHsbREvWYH2pXrRtrh4hHNgEo3e/8d11iWrLv5i35j4R8HHg9O0gFx1Tft5chqobtK7DzOkMbyHCEAAFp86REitvc+Sr3XvYmuk8VLM7kBB9kXNmh0ufZbkmhGrD+wDbSrLTmwogTL0Qou8yOfHKRISm2IHKL4jwHmKzhI63ZVpg==",
            "orderNumber": null,
            "orderSource": null,
            "petBirthDate": "20200613T230000Z",
            "petExternalId": null,
            "petInfo": null,
            "petMedications": null,
            "petName": "Fido",
            "petNeutered": false,
            "petSex": "Male",
            "petWeight": 10.1,
            "pharmacyNotes": null,
            "prescriptionId": 1183,
            "productName": "ELANCO DEFAULT RX Item",
            "readAttempts": "2",
            "refills": 11,
            "shippingAddress": null,
            "shippingCity": null,
            "shippingCountry": null,
            "shippingState": null,
            "shippingTotal": null,
            "shippingZip": null,
            "siteId": 40001081,
            "skuId": "RXMEDST2PK",
            "unitsRemaining": 12,
            "usageInstructions": "USE AS DIRECTED BY VETERINARIAN",
            "vetEmail": "test@vetsource.com",
            "vetExternalId": null,
            "vetFirstName": "Alan",
            "vetLastName": "Miller",
            "writtenDate": "20220613T230000Z"
        }
    }
  ```
