---
title: "ScriptShare Events"
linkTitle: "ScriptShare Events"
weight: 200
date: 2020-04-01
description: >
  Tracking real-time ScriptShare creation
---

You can track when a ScriptShare (or Recommendation) is created from ScriptRight.  This gives you the ability to share these recommendations with your pet-owner in your platform. We provide a simple mechanism for this via our Order ScriptShare API.

#### Sample events

With the event mechanism, you can receive notifications on the following Order events:

* ScriptShare Created

Sample ScriptShare Event Response:
```
 {
   "eventResponse":{
      "billingAddress":null,
      "billingCity":null,
      "billingCountry":null,
      "billingState":null,
      "billingZip":null,
      "customerEmail":"omistry+share@vetsource.com",
      "customerExternalId":null,
      "customerFirstName":"Oly",
      "customerLastName":"Mistry",
      "eventAction":"RXCART_SCRIPTSHARE_CREATED",
      "eventDateTime":"2018-12-19 16:25:57.241",
      "eventType":"RXCART",
      "lineItems":[
         {
            "autoShip":false,
            "cancellationReason":null,
            "discountAmount":null,
            "discountReason":null,
            "externalPetId":null,
            "hospitalDescription":"Test Site 1 for VetSource",
            "hospitalExternalId":"IND:100634",
            "hospitalExternalRef1":"100634",
            "hospitalExternalRef2":null,
            "hospitalNumber":100634,
            "orderItemNumber":"1461",
            "petId":20464619,
            "petName":"dingdong",
            "productName":"Baytril Otic",
            "productSku":"28090420BO",
            "quantity":1,
            "recommendedByVetFirstName":"Alan",
            "recommendedByVetId":2000241,
            "recommendedByVetLastName":"Miller",
            "recommendedByVetUserId":22954,
            "rxRefillCount":0,
            "shippingCost":null,
            "subTotal":null,
            "taxes":null,
            "totalPrice":null,
            "trackingNumber":null
         }
      ],
      "messageId":null,
      "orderNumber":null,
      "orderSource":null,
      "readAttempts":null,
      "recommendationDate":"Wed Dec 05 20:21:08 UTC 2018",
      "recommendationMailCount":null,
      "rxCartId":705,
      "shippingAddress":null,
      "shippingCity":null,
      "shippingCountry":null,
      "shippingState":null,
      "shippingTotal":null,
      "shippingZip":null,
      "status":"UNPROCESSED"
   }
}
  ```
