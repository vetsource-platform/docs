---
title: "Catalog Export"
linkTitle: "Catalog Export"
weight: 500
date: 2020-04-01
description: >
  Accessing your catalog via regular exports to a Google Storage Bucket
---

### Initial Setup

In order to enable catalog export, please contact your [Technical Contact](/docs/working-with-vetsource/contacts).  They will need to know your preferred schedule for catalog export, including:

1. Frequency - how often the export should occur, usually weekly.
1. Time - the exact time of day for the export to occur.  We prefer this to be during working hours so we can address potential issues quickly. E.g. 'Mondays at 7am CST'.

Prior to enabling the catalog export, you should ensure your catalog is set up with the items you want to be sold by you.

### Storage bucket

We create a [Google Storage Bucket](https://cloud.google.com/compute/docs/disks/gcs-buckets) (named `vetsource-external-<XXX>`) where we export the catalog files. We provide you a JSON file containing a token to access the bucket.

Files are kept in the bucket for 31 days, at which point they will be removed.

### Catalog export files

Catalog export files are zip files with the date and time of the export as part of the name (e.g. `catalog_export_2019-06-10_09-35-42.zip`). This zip file includes:

* Product CSV
* Option CSV
* Category CSV
* Files directory containing product images

The first zip file we provide will be the complete catalog containing all products that are marked as sold by you. All other files contain products that have changed since the previous export. All these files must be processed in order so that your catalog will be in sync with the Vetsource catalog.

### Accessing the catalog export

You will need to download [gsutil](https://cloud.google.com/storage/docs/gsutil) and run the following command:

```shell
gcloud auth activate-service-account --key-file <file name>.json --runtime nodejs61
```

where `<file name>` is the name of the token file provided to you.

You now have access to the Google Storage Bucket containing all catalog export files.

To list contents of your bucket:

```shell
gsutil ls gs://vetsource-external-<XXX>
```

To copy a file from your bucket to your current directory:

```shell
gsutil cp gs://vetsource-external-<XXX>/catalog_export_FILE_TIMESTAMP.zip
```


