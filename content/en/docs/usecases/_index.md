---
title: "Use Cases"
linkTitle: "Use Cases"
weight: 300
date: 2020-05-22
description: >
  Building solutions using the Vetsource Platform API
---

The Vetsource Platform enables a number of capabilities (placing orders, scheduling autoships, etc) that can be leveraged independently or together.  These capabilities give you the flexibility to build solutions that are unique to your needs.

While you may mix and match capabilities, get started by picking your preferred use case.