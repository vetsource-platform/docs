---
title: "Order Creation with Prescription"
linkTitle: "Order Creation with Prescription"
weight: 100
date: 2020-04-01
mermaid: true
description: >
  Send orders from your PIM with an approved prescription
---

Orders and prescriptions are independent objects in the Vetsource system sent in separate calls.  The Vetsource system will match orders for a specific product/pet combination with an active prescription for the same product/pet with available fills.  When a match is found, the order is moved into the Vetsource fulfillment system.  

### Order Creation Sequence

{{<mermaid>}}
sequenceDiagram
    participant Your System
    participant Vetsource
	participant Vetsource Fulfillment
	participant Pet Parent
    Your System ->> Vetsource: Get shipping method
	Your System ->> Vetsource: Create prescription
	Your System ->> Vetsource: Create order
	Vetsource ->> Vetsource Fulfillment: Order moves to Vetsource fulfillment
	Vetsource Fulfillment ->> Pet Parent: Order delivered to pet parent
{{< /mermaid >}}

### REST Calls

Below is the list of REST calls to create orders and prescriptions in the Vetsource system.  

#### Get Shipping Method

Standard or expedited shipping options are available for your order. Not all shipping options are available for every product/shipping address combination.  The Shipping Options API call returns a list of allowed shipping options determined by the shipping address and line items on the order.  [Get Shipping Method Example](/docs/api-examples/get-shipping-method/)

#### Create Prescription

Some orders require approved prescriptions before the order can be fulfilled. Prescriptions can be sent explicitly ahead of the order to ensure an order goes directly to the Vetsource pharmacy for fulfillment. In order to meet legal requirements, some specific veterinarian data must included in this call.    

A Prescription can be created with multiple refills. An order received by Vetsource when a prescription already exists with available fills for an line item's pet and product will go directly to the Vetsource pharmacy for fulfillment. A new Prescription is not required in this scenario.  [Create Prescription Example](/docs/api-examples/create-prescription/)

#### Create Order

Once the shipping option has been determined and a prescription is available, the order can be sent to Vetsource.  The system will look for an active prescription for the the product/pet on the order.  If a prescription is found the order is sent to the Vetsource fulfillment system. If a prescription with available fills is not found, the order will be placed in an "Awaiting Approval" status. [Create Order Example](/docs/api-examples/create-order/)  

#### Get Awaiting Approval Items

If an order is received by Vetsource and an active prescription with available fills is not found, the order will be in “Awaiting Approval” status.  The order will not move into Vetsource Fulfillment until a prescription is received.  The Order - Awaiting Approval call will return a list of all line items in the "Awaiting Approval" status.  Use this information to send us a prescription.  Once the prescription is received, the order will move into Vetsource fulfillment. [Get Awaiting Approval Items Example](/docs/api-examples/get-awaiting-approval-items/)  

#### Get Order Status

At any time you can retrieve the current status of an order. The order status can be retrieved using the Vetsource Order ID or your order ID. [Get Order Status Example](/docs/api-examples/get-order-status/)    
