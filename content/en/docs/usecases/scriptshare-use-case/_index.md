---
title: "Product Recommendation"  
linkTitle: "Product Recommendation"  
weight: 200
date: 2020-05-22
mermaid: true
description: >
  Send pre-approved prescription to a pet owner
---

The Vetsource system allows a DVM to proactively recommend products to their pet owners using a Vetsource Scriptshare.  

One use case example could be:  

- A pet owner visits a DVM
- DVM orders a test that will take some time to receive the results
- The pet owner goes home
- Based on the test results the DVM wishes to suggest prescription medication for the pet
- DVM recommends a product to the pet owner by creating a Scriptshare

### Scriptshare Sequence

{{<mermaid>}}
sequenceDiagram
    participant Your System
    participant Vetsource
    participant Pet Owner
    Your System ->> Vetsource: Create Scriptshare
    Vetsource ->> Pet Owner: Recommendation e-mail
    Pet Owner ->> Vetsource: Purchase Product
    Vetsource ->> Pet Owner: Ship product
{{< /mermaid >}}


### REST Calls

Below is the list of REST calls to manage Scriptshares in the Vetsource system.  

#### Create Scriptshare

Creating a Scriptshare will result in sending an e-mail to a pet owner recommending the purchase of a specific product.  The e-mail includes a link that directs the pet owner to the hospital's eCommerce site placing the recommended product into the cart ready for purchase.  If the DVM and Rx information is included in the REST call the Rx is preapproved so when the pet owner places the order, the order will go directly into the Vetsource pharmacy for fulfillment.  Up to 4 e-mails will be sent to the pet owner over a 7 day period until the product is purchased or the pet owner chooses to stop the e-mails. There is also the option when creating the Scriptshare to send the first e-mail to the pet owner on a date in the future.

 [Create Scriptshare Example](/docs/api-examples/create-scriptshare/)

#### Get Scriptshare

Retrieve Scriptshare information including status information.  The status information will include if the Scriptshare is still active and how many e-mails have been sent.  

 [Get Scriptshare Example](/docs/api-examples/get-scriptshare/)

#### Cancel Order

A Scriptshare can be cancelled using this REST call.  

 [Cancel Scriptshare Example](/docs/api-examples/cancel-scriptshare/)  

