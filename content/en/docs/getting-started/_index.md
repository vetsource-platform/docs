---
title: "Getting Started"
linkTitle: "Getting Started"
weight: 200
date: 2020-04-01
description: >
  Where and how to begin
---

Before using the Vetsource Platform APIs we need to enable API access for you.  Please contact your [Account Manager](/docs/working-with-vetsource/contacts) to get started.

### Access API Documentation

API Documentation is available to help you get started with your integration with Vetsource. The API documenation is hosted in GitLab using GitLab authentication. To gain access to the documentation:

1. Provide Vetsource with a GitLab user name for each user you wish to have access to the docs.
1. Once Vetsource has set up access for these users, they will be able to access the docs [Vetsource API Documentation](/)

### Test environments

Vetsource can create a test environment for you to access during initial development. This will allow you to develop and test your code without creating real orders.

Once Vetsource has provided you access to the API documentation we will provide the following:

1. URL to be used for test system access
1. Authorization token to provide access to test API endpoints
1. A practiceApiId value for a test practice in the test system

### IDs

Partner and Vetsource IDs play in important role in a successful integration.  See the next topic for more information.