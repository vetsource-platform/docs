---
title: "IDs"
linkTitle: "IDs"
weight: 100
date: 2020-04-01
description: >
  Working with IDs in the Vetsource Platform
---

Most objects in the Vetsource Platform have both an internal ID and an external ID. This allows easy synchronization between systems.  

All APIs in the Vetsource Platform leverage IDs for lookups and relationships. Every object in the system is created with a unique internal Vetsource ID and exposes a flexible external ID for your usage. This allows you to refer to objects via either the internal or external IDs for your convenience.

If your PIM is integrated with Vetsource, the PIM IDs will already be stored as the external ID with the Vetsource objects.  In some cases when we receive an API call where the external ID is not found in the Vetsource system, we will create a new object with this external ID using the data in the API call.   
 
#### ID Naming Convention

External ID: `xxxId` where `xxx` references the object type.  For example `orderId`, `clientId`, `petId`   
Internal ID: `xxxApiId` where `xxx` references the object type.  For example `orderApiId`, `clientApiId`, `petApiId`
