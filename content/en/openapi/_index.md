---
title: "API"
linkTitle: "API"
type: swagger
date: 2021-12-14
weight: 300
menu:
  main:
    weight: 300
---

{{< swaggerui src="/docs/api/openapi.yaml" >}}
